package sbu.cs;

import java.io.Serializable;
import java.util.List;
import java.util.ArrayList;
import java.util.Scanner;

public class Data implements Serializable {

    private final List<User>   UserList;
    private final List<String> TweetIDList;
    private final List<String> UsernameList;

    public Data() {
        UserList     = new ArrayList<>();
        TweetIDList = new ArrayList<>();
        UsernameList = new ArrayList<>();
    }

    public List<String> getUsernameList() {
        return this.UsernameList;
    }
    public List<String> getTweetIDList() {
        return TweetIDList;
    }

    public void addUser(User user)
    {
        UsernameList.add( user.getUsername() );
        UserList.add(user);
    }

    public void deleteUser(User user)
    {
        UserList.remove(user);
        UsernameList.remove(user.getUsername());
    }

    public boolean checkUser(User user)
    {
        boolean flag = false;

        for (User x : UserList) {
            if ( x.equals(user) ) {
                flag = true;
                break;
            }
        }

        return flag;
    }

    public boolean checkUsername(String username)
    {
        boolean flag = false;

        if (UsernameList.contains(username)) {
            flag = true;
        }

        return flag;
    }

    public User getUser(String username)
    {
        for (User user : UserList)
        {
            if (user.getUsername().equals(username)) {
                return(user);
            }
        }

        return null;
    }

    public static int getTweetNumFromID (String ID)
    {
        String[] str = ID.split("-");
        return Integer.parseInt(str[1]) - 1;
    }

    public static String getUsernameFromID(String ID)
    {
        String[] str = ID.split("-");
        return str[0];
    }

    public boolean checkIfTweetExists(String ID)
    {
        boolean flag = false;

        if ( ID.matches("\\S+-[0-9]+")  )
        {
            String username = getUsernameFromID(ID);
            int tweetNum = getTweetNumFromID(ID);

            if ( UsernameList.contains(username) ) {
                flag = ( tweetNum < getUser(username).getTweetCount() && tweetNum >= 0 );
            }
        }

        return flag;
    }

}
