package sbu.cs;

import java.io.*;

public abstract class SaveFile
{
    public static void WriteToFile(Data data)
    {
        try {
            FileOutputStream fileOut = new FileOutputStream("Twitter.dat");
            ObjectOutputStream objOut = new ObjectOutputStream(fileOut);

            objOut.writeObject(data);

            objOut.close();
            fileOut.close();

        } catch (FileNotFoundException e) {
            System.out.println("File not found");
        } catch (IOException e) {
            System.out.println("Error initializing stream");
        }
    }

    public static Data ReadFromFile()
    {
        Data data = new Data();

        try {
            FileInputStream fileIn = new FileInputStream("Twitter.dat");
            ObjectInputStream objIn = new ObjectInputStream(fileIn);

            data = (Data) objIn.readObject();

            objIn.close();
            fileIn.close();

        } catch (FileNotFoundException e) {
            System.out.println("File not found");
        } catch (IOException e) {
            System.out.println("Error initializing stream");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        return data;
    }

    public static Data LoadData()
    {
        Data data = new Data();
        File tmp = new File("Twitter.dat");

        if ( tmp.exists() ) {
            System.out.println("Data File Exists.");
            data = ReadFromFile();
        }
        else System.out.println("Data File Not Found. New Data File Created.");

        return data;
    }

}