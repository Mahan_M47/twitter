package sbu.cs;

import java.io.Serializable;
import java.util.List;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Scanner;

public class User implements Serializable
{
    private final List<Tweet> userTweets;
    private final List<User> followers, following;

    private String username, password, bio;
    private LocalDate joinDate;
    private int tweetCount;

    public User () {
        userTweets = new ArrayList<>();
        followers  = new ArrayList<>();
        following  = new ArrayList<>();
        tweetCount = 0;
        bio = "";
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) { this.password = password; }

    public String getUsername() {
        return username;
    }

    public LocalDate getJoinDate() {
        return joinDate;
    }

    public void setJoinDate() { this.joinDate = LocalDate.now(); }

    public void newTweet(Data data)
    {
        this.tweetCount++;
        Tweet tweet = new Tweet (this.username, this.tweetCount);

        System.out.println("Enter Your Tweet Text (must be less than 140 characters)");
        Scanner in = new Scanner(System.in);
        String text = in.nextLine();

        if (text.length() <= 140)
        {
            tweet.setText(text);
            userTweets.add(tweet);
            data.getTweetIDList().add( tweet.getID() );

            System.out.println("Done!");
        }

        else {
            this.tweetCount--;
            System.out.println("Your Tweet MUST Be Less Than 140 Characters Long.");
        }
    }

    public List<Tweet> getUserTweets() {
        return this.userTweets;
    }

    public int getTweetCount() {
        return this.tweetCount;
    }

    public List<User> getFollowers() {
        return this.followers;
    }

    public void addFollower(User follower) {
        this.followers.add(follower);
    }

    public List<User> getFollowing() {
        return this.following;
    }

    public void addFollowing(Data data)
    {
        Scanner in = new Scanner(System.in);

        System.out.println("Enter a username:");
        String username = in.next().toLowerCase();

        if ( data.checkUsername(username) )
        {
            if ( this.following.contains( data.getUser(username) ) ) {
                System.out.println("You're Already Following " + username + " .");
            }

            else if ( this.username.equals(username) ) {
                System.out.println("You Can't Follow Yourself.");
            }

            else {
                this.following.add( data.getUser(username) );
                data.getUser(username).addFollower(this);

                System.out.println("You're Now Following " + username + " !");
            }
        }

        else {
            System.out.println("This User Does Not Exist.");
        }
    }

    public void unfollow(Data data)
    {
        Scanner in = new Scanner(System.in);

        System.out.println("Enter a Username:");
        String username = in.next().toLowerCase();

        if ( data.checkUsername(username) )
        {
            if ( username.equals(this.username) ) {
                System.out.println("You Can't Unfollow Yourself.");
            }

            else if (! this.following.contains(data.getUser(username)) ) {
                System.out.println("You're Not Following " + username + " .");
            }

            else {
                this.following.remove( data.getUser(username) );
                data.getUser(username).followers.remove(this);

                System.out.println("User " + username + " Has Been Unfollowed.");
            }
        }

        else {
            System.out.println("This User Does Not Exist.");
        }

    }

    public void likeTweet(int tweetNum, String likedBy)
    {
        if ( likedBy.equals(this.username) ) {
            System.out.println("You Can't Like Your Own Tweets.");
        }

        else if ( userTweets.get(tweetNum).getLikes().contains(likedBy) ) {
            System.out.println("You Have Already Liked This Tweet.");
        }

        else {
            userTweets.get(tweetNum).addLike(likedBy);
            System.out.println("Done!");
        }

    }

    public void replyToTweet(int tweetNum, String repliedToBy)
    {
        System.out.println("Enter Your Reply Text (must be less than 140 characters)");
        Scanner in = new Scanner(System.in);
        String text = in.nextLine();

        if (text.length() <= 140) {
            userTweets.get(tweetNum).addReply(repliedToBy, text);
            System.out.println("Done!");
        }

        else System.out.println("Your Reply MUST Be Less Than 140 Characters Long.");
    }

    public String getBio() {
        return this.bio;
    }

    public void setBio()
    {
        System.out.println("Your Current Bio Is:" + this.bio + "\nEnter Your New Bio Text: ");
        Scanner in = new Scanner(System.in);
        this.bio =  in.nextLine();
        System.out.println("Done!");
    }

    public boolean equals(User user)
    {
        boolean flag = false;

        if (this.username.equals(user.username) && this.password.equals(user.password)) {
            flag = true;
        }
        return flag;
    }

}
