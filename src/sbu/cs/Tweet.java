package sbu.cs;

import java.io.Serializable;
import java.util.List;
import java.util.ArrayList;
import java.util.Date;

public class Tweet implements Comparable <Tweet>, Serializable
{

    private final List<String> likedBy, replies;
    private final String ID;
    private String text;
    private Date DateTime;

    public Tweet(String username, int tweetNum)
    {
        this.ID = generateID(username, tweetNum);
        this.likedBy = new ArrayList<>();
        this.replies = new ArrayList<>();
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
        this.DateTime = new Date();
    }

    public String getLikes()
    {
        StringBuilder str = new StringBuilder();
        str.append( this.likedBy.size() );

        if (this.likedBy.size() != 0)
        {
            str.append("  Liked By:");

            for (String username : this.likedBy) {
                str.append(" ").append(username).append(",");
            }
            str.deleteCharAt( str.length() - 1 );
            str.append(" .");
        }

        return str.toString();
    }

    public void addLike(String likedBy) {
        this.likedBy.add(likedBy);
    }

    public Date getDateTime() {
        return DateTime;
    }

    public String getID() {
        return this.ID;
    }

    public static String generateID (String username, int tweetNum)
    {
        return username + "-" + tweetNum;
    }

    public int getReplyCount() {
        return replies.size();
    }

    public String getReplies()
    {
        StringBuilder str = new StringBuilder();

        for (String reply : replies)
        {
            str.append(reply).append("\n");
        }

        return str.toString();
    }

    public void addReply (String repliedToBy, String text) {
        replies.add("    " + repliedToBy + ": " + text);
    }

    @Override
    public int compareTo(Tweet tweet)
    {
        return getDateTime().compareTo(tweet.getDateTime());
    }

}
