package sbu.cs;

import java.util.Scanner;

public abstract class Profile {

    public static void showProfile (User user)
    {
        System.out.println("\n     Username: " + user.getUsername() + "\n     Date Joined: " + user.getJoinDate() +
                "\n     " + user.getTweetCount() + " Tweet(s)   " + user.getFollowers().size() + " Followers   " +
                user.getFollowing().size() + " Following");

        if (! user.getBio().equals("") ) {
            System.out.println("Bio: " + user.getBio());
        }

        Timeline.printLine();

        if (user.getTweetCount() == 0) {
            System.out.println("This User Doesn't Have Any Tweets.");
        }
        else for (int i = user.getTweetCount() - 1; i >= 0; i--)
        {
            Tweet tweet = user.getUserTweets().get(i);

            System.out.println( "ID: " + tweet.getID() + "  " + tweet.getDateTime() + "\n   " + tweet.getText() +
                    "\nLikes: " + tweet.getLikes() );

            if (tweet.getReplyCount() > 0) {
                System.out.print( "Replies:\n" + tweet.getReplies() );
            }

            Timeline.printLine();
        }
    }

    public static void userProfile (Data data)
    {
        System.out.println("Enter a Username: ");
        Scanner in = new Scanner(System.in);
        String username = in.nextLine();

        if ( data.checkUsername(username) ) {
            showProfile( data.getUser(username) );
        }
        else System.out.println("This User does not exist.");
    }
}
