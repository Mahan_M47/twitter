package sbu.cs;

import java.util.List;
import java.util.ArrayList;

public abstract class Timeline
{
    public static void generateTimeline(User user)
    {
        List<Tweet> tweets = new ArrayList<>();

        for ( User following : user.getFollowing() ) {
            for ( Tweet tweet : following.getUserTweets()) {
                tweets.add(tweet);
            }
        }

        for ( Tweet tweet : user.getUserTweets()) {
            tweets.add(tweet);
        }

        tweets = sortTweets(tweets);

        if (tweets.size() == 0) {
            System.out.println("Your Timeline Is Empty!");
        }
        else
        {
            System.out.println();

            for (Tweet tweet : tweets)
            {
                System.out.println( "ID: " + tweet.getID() + "  " + tweet.getDateTime() + "\n   " +
                        Data.getUsernameFromID( tweet.getID() ) + ": " + tweet.getText() +
                        "\nLikes: " + tweet.getLikes() );

                if (tweet.getReplyCount() > 0) {
                    System.out.print( "Replies:\n" + tweet.getReplies() );
                }

                printLine();
            }
        }

    }

    public static List<Tweet> sortTweets (List<Tweet> tweets)
    {
        Tweet tmp;
        int size = tweets.size();

        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size - 1 - i; j++) {
                if ( tweets.get(j).compareTo(tweets.get(j + 1)) < 0)
                {
                    tmp = tweets.get(j);
                    tweets.set( j , tweets.get(j + 1) );
                    tweets.set( j + 1 , tmp );
                }
            }
        }

        return tweets;
    }

    public static void printLine () {
        System.out.println("\n_______________________________________________________________________________________\n");
    }

}
