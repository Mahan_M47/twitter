package sbu.cs;

public class App {

    public static void main(String[] args)
    {
        System.out.println("Welcome to TWITTER!");

        Data data = SaveFile.LoadData();

        Menu menu = new Menu(data);
        menu.mainMenu();
    }

    public static void exitProgram(Data data)
    {
        System.out.println("You Have Exited The Program.");

        SaveFile.WriteToFile(data);
        System.exit(0);
    }

}
