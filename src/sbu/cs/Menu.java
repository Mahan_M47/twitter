package sbu.cs;

import java.util.Scanner;

public class Menu {

    private static Data data;

    public Menu(Data data) {
        Menu.data = data;
    }

    public static void mainMenu()
    {
        System.out.println("\nChoose An Option By Entering a Number:\n1. Login\n2. Sign Up\n3. Exit");

        Scanner in = new Scanner(System.in);
        char action = in.next().charAt(0);

        while (action != '3')
        {
            switch (action) {
                case '1':
                    loginMenu();
                    break;
                case '2':
                    signUpMenu();
                    break;
                default:
                    System.out.println("Please Enter a Valid Number.");
                    action = in.next().charAt(0);
            }
        }
        App.exitProgram(data);

    }

    public static void loginMenu()
    {
        Scanner in = new Scanner(System.in);
        User user = new User();

        System.out.println("Please Enter Your Username:");
        user.setUsername( in.next().toLowerCase() );

        if (! data.checkUsername(user.getUsername()) ) {
            System.out.println("Sorry, This Username Doesn't Exist.");
            mainMenu();
        }
        else
        {
            System.out.println("Please Enter Your Password:");
            user.setPassword( in.next() );

            if (data.checkUser(user))
            {
                System.out.println("Welcome Back " + user.getUsername() + " !");
                Commands( data.getUser(user.getUsername()) );
            }
            else
                {
                System.out.println("Incorrect Password.");
                mainMenu();
            }
        }
    }

    public static void signUpMenu()
    {
        Scanner in = new Scanner(System.in);
        User newUser = new User();

        System.out.println("Please Enter a Username (Cannot Contain a SPACE or Hyphen):");
        String password, username = in.nextLine().toLowerCase();

        if ( username.contains("-") || username.contains(" ") ) {
            System.out.println("Username Cannot Contain a Hyphen (' - ') or SPACE.");
            mainMenu();
        }
        else newUser.setUsername(username);

        if ( data.checkUsername(newUser.getUsername()) ) {
            System.out.println("Sorry, This Username Has Already Been Taken.");
        }
        else {
            System.out.println("Please Enter a Password (Cannot Contain a SPACE):");
            password = in.nextLine();

            if ( password.contains(" ") ) {
                System.out.println("Password Cannot Contain a SPACE.");
                mainMenu();
            }
            else {
                newUser.setPassword(password);
                newUser.setJoinDate();

                data.addUser(newUser);
                System.out.println("Congrats!");
                SaveFile.WriteToFile(data);
            }
        }
        mainMenu();
    }

    public static void Commands(User loggedUser)
    {
        System.out.println("Type /? to Display the Commands List.\nEnter a Command:");

        Scanner in = new Scanner(System.in);
        String ID, command = in.nextLine().toLowerCase();

        while (! command.equals("/logout") )
        {
            switch (command) {
                case "/?":
                    showCommandsList();
                    command = in.nextLine().toLowerCase();
                    break;

                case "/tweet":
                    loggedUser.newTweet(data);
                    command = in.nextLine().toLowerCase();
                    break;

                case "/my profile":
                    Profile.showProfile(loggedUser);
                    command = in.nextLine().toLowerCase();
                    break;

                case "/show profile":
                    Profile.userProfile(data);
                    command = in.nextLine().toLowerCase();
                    break;

                case "/follow":
                    loggedUser.addFollowing(data);
                    command = in.nextLine().toLowerCase();
                    break;

                case "/unfollow":
                    loggedUser.unfollow(data);
                    command = in.nextLine().toLowerCase();
                    break;

                case "/my followers":
                    if (loggedUser.getFollowers().size() == 0) {
                        System.out.println("You don't have any followers.");
                    }
                    else for (User u : loggedUser.getFollowers()) {
                        System.out.println( u.getUsername() );
                    }
                    command = in.nextLine().toLowerCase();
                    break;

                case "/my following":
                    if (loggedUser.getFollowing().size() == 0) {
                        System.out.println("You haven't followed anyone yet.");
                    }
                    else for (User u : loggedUser.getFollowing()) {
                        System.out.println(u.getUsername());
                    }
                    command = in.nextLine().toLowerCase();
                    break;

                case "/all users":
                    for (String username : data.getUsernameList() ) {
                        System.out.print(username + " | ");
                    }
                    System.out.println();

                    command = in.nextLine().toLowerCase();
                    break;

                case "/timeline":
                    Timeline.generateTimeline(loggedUser);
                    command = in.nextLine().toLowerCase();
                    break;

                case "/like":
                    System.out.println("Enter the ID of the Tweet You Want to Like.");
                    ID = in.nextLine();

                    if ( data.checkIfTweetExists(ID) ) {
                        data.getUser( Data.getUsernameFromID(ID) ).likeTweet( Data.getTweetNumFromID(ID), loggedUser.getUsername() );
                    }
                    else System.out.println("This Tweet ID Does Not Exist.");

                    command = in.nextLine().toLowerCase();
                    break;

                case "/reply":
                    System.out.println("Enter the ID of the Tweet You Want to Reply to:");
                    ID = in.nextLine();

                    if ( data.checkIfTweetExists(ID) ) {
                        data.getUser( Data.getUsernameFromID(ID) ).replyToTweet( Data.getTweetNumFromID(ID), loggedUser.getUsername() );
                    }
                    else System.out.println("This Tweet ID Does Not Exist.");

                    command = in.nextLine().toLowerCase();
                    break;

                case "/set bio":
                    loggedUser.setBio();
                    command = in.nextLine().toLowerCase();
                    break;

                case "/delete account":
                    System.out.println("Are You Sure You Want to Delete Your Account? Type \"DELETE\" to Confirm.");

                    if ( in.nextLine().equalsIgnoreCase("DELETE") ) {
                        data.deleteUser(loggedUser);
                        System.out.println("Your Account Has Been Deleted. You Will Be Logged Out.");
                        command = "/logout";
                    }
                    else {
                        System.out.println("Your Account Was NOT Deleted.");
                        command = in.nextLine().toLowerCase();
                    }
                    break;

                default:
                    System.out.println("Please Enter a Valid Command. Type /? to Display the Commands List.");
                    command = in.nextLine().toLowerCase();
            }

            SaveFile.WriteToFile(data);
        }

        mainMenu();
    }

    public static void showCommandsList()
    {
        System.out.println("/tweet\n" + "/my profile\n" + "/logout\n" + "/follow\n" + "/unfollow\n" +
                "/my followers\n" + "/my following\n" + "/all users\n" + "/timeline\n" + "/like\n" +
                "/reply\n"  + "/set bio\n" + "/delete account\n");
    }
}
